from django.db import models

"""
 Note that this app is by no means a part of the system, it is only created
 to simulate or act as osim API, if osim provides us with information implemented here
 all these models shall be removed
"""


class User(models.Model):

    # choices for user type
    LECTURER = 'lecturer'
    STUDENT = 'student'

    USER_TYPE = [
        (LECTURER, 'LECTURER'),
        (STUDENT, 'STUDENT')
    ]

    registration_number = models.IntegerField()
    user_type = models.CharField(max_length=10, choices=USER_TYPE, default=LECTURER)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    picture = models.ImageField(upload_to='users/profile/pictures/%Y/%m/%d')

    users = models.Manager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'
    
    def get_full_name(self):
        return "%s  %s %s"%(self.first_name, self.last_name, self.registration_number)

    def __str__(self):
        return self.get_full_name()


class StudentInfo(models.Model):
    """
        This is only because lectures do not have the information on this table
    """

    user = models.OneToOneField(User, related_name="information", related_query_name='student', on_delete=models.CASCADE)

    # define level of education eg:- bachelor or diploma

    BACHELOR = 'bachelor'
    DIPLOMA = 'diploma'
    MASTERS = 'masters'
    COMPUTER_ENGINEERING = 'computer engineering'
    CIVIL_ENGINEERING = 'civil engineering'
    MINING_ENGINEERING = 'mining engineering'

    STUDENT_LEVEL = [
        (BACHELOR, 'BACHELOR'),
        (DIPLOMA, 'DIPLOMA'),
        (MASTERS, 'MASTERS')
    ]

    level = models.CharField(max_length=255, choices=STUDENT_LEVEL, default=BACHELOR)

    # year of study
    slug = models.IntegerField()

    # dummy list of courses
    STUDENT_COURSE = [
        (COMPUTER_ENGINEERING, 'COMPUTER ENGINEERING'),
        (CIVIL_ENGINEERING, 'CIVIL ENGINEERING'),
        (MINING_ENGINEERING, 'MINING ENGINEERING')

    ]

    course = models.CharField(max_length=255, choices=STUDENT_COURSE, default=COMPUTER_ENGINEERING)
    stream = models.IntegerField()
    identity = models.CharField(max_length=10, null=True, blank=True, editable=False)

    information = models.Manager()

    class Meta:
        verbose_name = 'Student information'
        verbose_name_plural = 'Student information'
    
    def __str__(self):
        return self.user.get_full_name()
    
    def save(self, *args, **kwargs):

        # identify students course
        if self.level == self.BACHELOR:
            self.identity = "BENG-%s-%s"%(self.slug, self.stream)
        elif self.level == self.DIPLOMA:
            self.identity = "OD-%s-%s"%(self.slug, self.stream)
        else:
            self.identity = "MS-%s-%s"%(self.slug, self.stream)

        super(StudentInfo, self).save(*args, **kwargs)

from django.urls import path
from osimAPI import views

urlpatterns = [
    # validate users from osim
    path('validate/user/<int:reg_number>', views.validate_user)
]


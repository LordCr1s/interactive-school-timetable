from django.apps import AppConfig


class OsimapiConfig(AppConfig):
    name = 'osimAPI'

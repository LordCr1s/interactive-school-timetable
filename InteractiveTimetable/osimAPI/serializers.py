from osimAPI.models import User, StudentInfo
from rest_framework import serializers

class StudentInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = StudentInfo
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):

    information = StudentInfoSerializer()
    class Meta:
        model = User
        fields = '__all__'
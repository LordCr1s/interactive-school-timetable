from django.shortcuts import render
from rest_framework.decorators import api_view
from osimAPI.models import User
from rest_framework.response import Response
from rest_framework import status
from osimAPI.serializers import UserSerializer, StudentInfoSerializer
from accounts.models import CustomUser


@api_view(['GET', ])    
def validate_user(request, reg_number):
    
    # check if user is already registered on the itms system
    try:
        CustomUser.users.get(registration_number=reg_number)

        # user can not regiser twice
        return Response(status=status.HTTP_403_FORBIDDEN)
    except CustomUser.DoesNotExist:

        # validate user from osim database
        try:
            user = User.users.get(registration_number=reg_number)
            serializer = UserSerializer(user)

            return Response(status=status.HTTP_200_OK, data=serializer.data)

        except User.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
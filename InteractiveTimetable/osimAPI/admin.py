from django.contrib import admin
from osimAPI.models import User, StudentInfo

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('registration_number', 'first_name', 'last_name', 'user_type')

@admin.register(StudentInfo)
class StudentInfoAdmin(admin.ModelAdmin):
    list_display = ('user', 'identity', 'level', 'course', 'stream')


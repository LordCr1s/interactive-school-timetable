from django.contrib import admin
from notifyManager.models import Notification

@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = ('time_sent', 'session', 'content')  
from notifyManager.models import Notification
from rest_framework import serializers

class NotifySerializer(serializers.ModelSerializer):
    time_sent = serializers.TimeField(format="%H:%M")
    class Meta:
        model = Notification
        fields = '__all__'
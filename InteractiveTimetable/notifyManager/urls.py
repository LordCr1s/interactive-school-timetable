from django.urls import path
from notifyManager import views

urlpatterns = [
    # get / fetch session
    path('get/notification/<int:user_id>', views.get_notified),
    
    # update session PUT
    # path('create/notification', views.create_notification),
]
from django.apps import AppConfig


class NotifymanagerConfig(AppConfig):
    name = 'notifyManager'

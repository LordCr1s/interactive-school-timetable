from django.db import models
from sessionReschedule.models import Session

class Notification(models.Model):

    session = models.ForeignKey(Session, related_name='notifications', on_delete=models.CASCADE)
    time_sent = models.TimeField(auto_now=True)
    content = models.TextField()

    notifications = models.Manager()

    class Meta:
        db_table = 'notifications'
        verbose_name = 'Notification'
        verbose_name_plural = 'Notifications'

    def get_full_notification(self):
        return "%s  %s %s"%(self.session, self.time_sent, self.content)

    def __str__(self):
        return self.get_full_notification()
    
from django.shortcuts import render
from rest_framework.decorators import api_view
from notifyManager.models import Notification
from rest_framework.response import Response
from rest_framework import status
from notifyManager.serializers import NotifySerializer
from accounts.models import CustomUser


# Get Notifications
@api_view(['GET', ])
def get_notified(request, user_id):
    
    try:
        user = CustomUser.users.get(pk=user_id)
        

    except Notification.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

from django.contrib import admin
from sessionReschedule.models import Session, Rescheduler
from sessionReschedule.models import Module
from sessionReschedule.models import Timetable

@admin.register(Session)
class SessionAdmin(admin.ModelAdmin):
    list_display = ('module', 'venue', 'lecturer', 'start_time', 'end_time', 'day_of_the_week', 'timetable')

@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')

@admin.register(Timetable)
class TimetableAdmin(admin.ModelAdmin):
    list_display = ('level', 'year', 'course', 'stream')

@admin.register(Rescheduler)
class ReschedulerAdmin(admin.ModelAdmin):
    list_display = ('session', 'start_time', 'end_time', 'venue')

from django.db import models
from venueManager.models import Venue
from accounts.models import CustomUser as User


class Module(models.Model):
    code = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=255)

    modules = models.Manager()

    class Meta:
        db_table = 'Module'
        verbose_name = 'Module'
        verbose_name_plural = 'Modules'

    def get_full_module(self):
        return "%s  %s"%(self.code, self.name)

    def __str__(self):
        return self.get_full_module()


class Timetable(models.Model):

    BACHELOR = 'bachelor'
    DIPLOMA = 'diploma'
    MASTERS = 'masters'
    COMPUTER_ENGINEERING = 'computer engineering'
    CIVIL_ENGINEERING = 'civil engineering'

    STUDENT_LEVEL = [
        (BACHELOR, 'BACHELOR'),
        (DIPLOMA, 'DIPLOMA'),
        (MASTERS, 'MASTERS')
    ]

    STUDENT_COURSE = [
        (COMPUTER_ENGINEERING, 'COMPUTER ENGINEERING'),
        (CIVIL_ENGINEERING, 'CIVIL ENGINEERING')
    ]

    level = models.CharField(max_length=255, choices=STUDENT_LEVEL, default=BACHELOR)
    year = models.DateField(auto_now=True)
    course = models.CharField(max_length=255, choices=STUDENT_COURSE, default=COMPUTER_ENGINEERING)
    stream = models.IntegerField()

    timetables = models.Manager()

    class Meta:
        db_table = 'timetable'
        verbose_name = 'Timetable'
        verbose_name_plural = 'Timetables'

    def get_full_timetable_name(self):
        return "%s  %s %s %s"%(self.level, self.year, self.course, self.stream)

    def __str__(self):
        return self.get_full_timetable_name()

class Session(models.Model):

    MONDAY = 'monday'
    TUESDAY = 'tuesday'
    WEDNESDAY = 'wednesday'
    THURSDAY = 'thursday'
    FRIDAY = 'friday'

    DAY_OF_THE_WEEK = [
        (MONDAY, 'MONDAY'),
        (TUESDAY, 'TUESDAY'),
        (WEDNESDAY, 'WEDNESDAY'),
        (THURSDAY, 'THURSDAY'),
        (FRIDAY, 'FRIDAY')
    ]

    TEMPORARY = 'temporary'
    PERMENENT = 'permenent'
    RECHEDULE_TYPES = [(TEMPORARY, 'temporary'), (PERMENENT, 'permenent')]

    timetable = models.ForeignKey(Timetable, related_name='sessions', related_query_name='session', on_delete=models.CASCADE)
    lecturer = models.ForeignKey(User, related_name='lecturers', related_query_name='lecturer', on_delete=models.CASCADE)
    module = models.ForeignKey(Module, related_name='module_names', related_query_name='module_name', on_delete=models.CASCADE)
    venue = models.ForeignKey(Venue, related_name='venue_names', related_query_name='venue_name', on_delete=models.CASCADE)
    start_time = models.TimeField()
    end_time = models.TimeField()
    day_of_the_week = models.CharField(max_length=255, choices=DAY_OF_THE_WEEK, default=MONDAY)
    is_rescheduled = models.BooleanField(default=False)
    rechedule_type = models.CharField(max_length=10, choices=RECHEDULE_TYPES, default=TEMPORARY)
   

    sessions = models.Manager()

    class Meta:
        db_table = 'session'
        verbose_name = 'Session'
        verbose_name_plural = 'Sessions'
        

    def get_session_fullname(self):
        return "%s at %s by %s"%(self.module, self.venue, self.lecturer)

    def __str__(self):
        return self.get_session_fullname()




class Rescheduler(models.Model):

    MONDAY = 'monday'
    TUESDAY = 'tuesday'
    WEDNESDAY = 'wednesday'
    THURSDAY = 'thursday'
    FRIDAY = 'friday'

    DAY_OF_THE_WEEK = [
        (MONDAY, 'MONDAY'),
        (TUESDAY, 'TUESDAY'),
        (WEDNESDAY, 'WEDNESDAY'),
        (THURSDAY, 'THURSDAY'),
        (FRIDAY, 'FRIDAY')
    ]

    session = models.OneToOneField(Session, related_name='rescheduled_session', on_delete=models.CASCADE)
    start_time = models.TimeField()
    end_time = models.TimeField()
    venue = models.ForeignKey(Venue, related_name='rescheduled_venue', on_delete=models.CASCADE)
    day_of_the_week = models.CharField(max_length=255, choices=DAY_OF_THE_WEEK, default=MONDAY)

    reschedulers = models.Manager()

    class Meta:
        db_table = 'rescheduler'
        verbose_name = 'Reschedule'
        verbose_name_plural =  "Reschedules"
from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from sessionReschedule.models import Session, Timetable, Module, Rescheduler
from venueManager.models import Venue
from rest_framework import status
from sessionReschedule.serializers import TimetableSerializer
from sessionReschedule.serializers import SessionSerializer
from accounts.models import CustomUser as User
from notifyManager.models import Notification


# Fetching a list of all sessions

@api_view(['GET', ])
def get_timetable(request):
    
    try:
        timetable = Timetable.timetables.get(course=request.GET['course'], level=request.GET['level'])
        serializer = TimetableSerializer(timetable)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    except Timetable.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['GET', ])
def show_sessions_by_venue(request, venue_id):
    try:
        venue = Venue.venues.get(pk=venue_id)
        sessions = Session.sessions.filter(venue=venue)

        serializer =SessionSerializer(sessions, many=True)
        return Response(status=status.HTTP_200_OK, data=serializer.data)
        
    except Venue.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['GET', ])
def show_sessions_by_lecturer(request, lecturer_id):
    try:
        lecturer = User.users.get(pk=lecturer_id)
        sessions = Session.sessions.filter(lecturer=lecturer)

        serializer =SessionSerializer(sessions, many=True)
        return Response(status=status.HTTP_200_OK, data=serializer.data)
        
    except User.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['POST', ])
def reschedule(request):
    
    # check the availability of a venue
    print(request.data)
    try:
        
        venue = Venue.venues.get(pk=request.data['venue_id'])
        if venue.status == 'Available':
            
            # reschedule session
            try:
                module = Module.modules.get(code=request.data['module_id'])
                session = Session.sessions.get(module=module, day_of_the_week=request.data['prev_day'])

                # check the type of reschedule
                if request.data['reschedule_type'] == 'permenent':
                    
                    # keep history of the last session records
                    Rescheduler.reschedulers.create(
                        session = session,
                        start_time = session.start_time,
                        end_time = session.end_time,
                        venue = venue,
                        day_of_the_week = session.day_of_the_week
                    ).save()

                    session_full_name = session.get_session_fullname()

                    # reschedule session
                    session.is_rescheduled = True
                    session.rechedule_type = request.data['reschedule_type']
                    session.start_time = request.data['start_time']
                    session.end_time = request.data['end_time']
                    session.day_of_the_week = request.data['day_of_the_week']
                    session.venue = venue
                    session.save() 

                    # notify user for session rescheduling
                    Notification.notifications.create(
                        session = session,
                        content = "%s was rescheduled to %s on %s at %s to %s"%(
                            session_full_name, 
                            session.venue, 
                            session.day_of_the_week, 
                            session.start_time, 
                            session.end_time
                        )
                    ).save()
                    return Response(status=status.HTTP_200_OK)
                    
                else:

                    # check for session collision


                    # permenet reschedule session
                    session.is_rescheduled = True
                    session.rechedule_type = request.data['reschedule_type']
                    session.start_time = request.data['start_time']
                    session.end_time = request.data['end_time']
                    session.day_of_the_week = request.data['day_of_the_week']
                    session.venue = venue
                    session.save()
                    return Response(status=status.HTTP_200_OK)

            except Session.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
    except Venue.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

@api_view(['GET', ])
def activate_session(request, session_id, action):

    try:
        session = Session.sessions.get(pk=session_id)
        if action == 1:
            session.venue.active_session = True
            session.venue.save()
        else:
            session.venue.active_session = False
            session.venue.save()

        return Response(status=status.HTTP_200_OK)  
    except Session.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
from sessionReschedule.models import Session, Module, Timetable, Venue
from accounts.models import CustomUser
from rest_framework import serializers
from notifyManager.serializers import NotifySerializer

class ModuleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Module
        fields = ('code', 'name')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name')

class VenueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venue
        fields = '__all__'  

class SessionSerializer(serializers.ModelSerializer):

    module = ModuleSerializer()
    venue = VenueSerializer()
    lecturer = UserSerializer()
    notifications = NotifySerializer(many=True)
    class Meta:
        model = Session
        fields = '__all__'

class TimetableSerializer(serializers.ModelSerializer):
    sessions = SessionSerializer(many=True)
    class Meta:
        model = Timetable
        fields = ('level', 'year', 'course', 'stream', 'timetables', 'sessions')
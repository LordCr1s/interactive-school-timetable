from django.urls import path
from sessionReschedule import views

urlpatterns = [
    # get / fetch session
    path('get/timetable', views.get_timetable),

    # show sessions by venue
    path('get/sessions/by/venue/<int:venue_id>', views.show_sessions_by_venue),

    # show sessions by lecturer
    path('get/sessions/by/lecturer/<int:lecturer_id>', views.show_sessions_by_lecturer),
    
    # reschedule sessions
    path('reschedule', views.reschedule),
    
    # activate session
    path('activate/session/<int:session_id>/<int:action>', views.activate_session)
]
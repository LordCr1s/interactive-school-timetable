from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    # osim api end point
    path('api/osim/', include('osimAPI.urls')),

    # venue api
    path('api/venue/', include('venueManager.urls')),

    # session reschedule
    path('api/session/', include('sessionReschedule.urls')),

    # notification manager
    path('api/notify/', include('notifyManager.urls')),

    # authentications end points
    path('api/auth/', include('accounts.urls')),

    # administrator page url
    path('admin/', admin.site.urls),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

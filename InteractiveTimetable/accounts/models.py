from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from time import strftime, gmtime
from django.utils import timezone
from django.conf import settings
from django.core.mail import send_mail
from django.db.utils import IntegrityError
from django.core.exceptions import ValidationError


class CustomManager(BaseUserManager):
    def _create_user(self, email=None, password=None, is_superuser=False, is_staff=False, is_active=False):
        if email is None:
            raise ValueError("users must have an email")
        if password is None:
            raise ValueError("users must have a password")

        user = self.model(
            email=self.normalize_email(email),
            is_superuser=is_superuser,
            is_staff=is_staff,
            is_active=is_active,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **kwargs):
        user = self._create_user(
            email, password, is_staff=False, is_superuser=False, is_active=False)
        user.first_name = kwargs['first_name']
        user.last_name = kwargs['last_name']
        user.user_type = kwargs['user_type']
        user.date_joined = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        user.save(using=self._db)
        return user

    def create_staffuser(self, email=None, password=None, **kwargs):
        user = self._create_user(
            email, password, is_staff=True, is_superuser=False, is_active=False)
        user.first_name = kwargs['first_name']
        user.last_name = kwargs['last_name']
        user.user_type = kwargs['user_type']
        user.date_joined = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **kwargs):
        user = self._create_user(
            email, password, is_superuser=True, is_staff=True, is_active=True)
        user.first_name = kwargs['first_name']
        user.last_name = kwargs['last_name']
        user.user_type = kwargs['user_type']
        user.date_joined = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    # choices for user type
    LECTURER = 'lecturer'
    STUDENT = 'student'

    USER_TYPE = [
        (LECTURER, 'LECTURER'),
        (STUDENT, 'STUDENT')
    ]

    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=60)
    last_name = models.CharField(max_length=60)
    registration_number = models.IntegerField(null=True)
    user_type = models.CharField(max_length=10, choices=USER_TYPE, default=LECTURER)

    date_joined = models.DateTimeField(default=timezone.now)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'user_type']

    users = CustomManager()

    class Meta:
        db_table = 'user'
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return "%s %s"%(self.first_name, self.last_name)

    def get_full_name(self):
        return self.first_name + ' ' + self.last_name

    def get_short_name(self):
        return self.last_name

    def get_username(self):
        return self.email


class UserProfile(models.Model):

    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    mobile_contact = models.CharField(max_length=255, null=True, blank=True)
    profile_image = models.ImageField(upload_to='users/images/%Y/%M/%d', blank=True, null=True, default='users/user.png')
    
    # define level of education eg:- bachelor or diploma

    BACHELOR = 'bachelor'
    DIPLOMA = 'diploma'
    MASTERS = 'masters'
    COMPUTER_ENGINEERING = 'computer engineering'
    CIVIL_ENGINEERING = 'civil engineering'
    MINING_ENGINEERING = 'mining engineering'

    STUDENT_LEVEL = [
        (BACHELOR, 'BACHELOR'),
        (DIPLOMA, 'DIPLOMA'),
        (MASTERS, 'MASTERS')
    ]

    level = models.CharField(max_length=255, choices=STUDENT_LEVEL, default=BACHELOR)
    slug = models.CharField(max_length=10, null=True, blank=True, editable=False)
    
    # dummy list of courses
    STUDENT_COURSE = [
        (COMPUTER_ENGINEERING, 'COMPUTER ENGINEERING'),
        (CIVIL_ENGINEERING, 'CIVIL ENGINEERING'),
        (MINING_ENGINEERING, 'MINING ENGINEERING')

    ]

    course = models.CharField(max_length=255, choices=STUDENT_COURSE, default=COMPUTER_ENGINEERING)
    stream = models.IntegerField(default=1)

    profiles = models.Manager()

    class Meta:
        db_table = 'user_profile'
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profiles'

    def __str__(self):
        return self.user.email
from django.shortcuts import get_object_or_404
from . import forms
from django.contrib.auth import authenticate, login, views
from django.core.mail import send_mail
import random, datetime
from django.utils import timezone
from .models import UserProfile, CustomUser
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.views import APIView
from django.contrib.auth import logout

from rest_framework import serializers
from rest_framework.response import Response

class RegistrationFormSerilizer(serializers.Serializer):
    email = serializers.EmailField()
    mobile_number = serializers.CharField(max_length=255)
    first_name = serializers.CharField(max_length=60)
    last_name = serializers.CharField(max_length=60)
    registration_number = serializers.IntegerField(default=0)
    user_type = serializers.CharField(max_length=10)
    password1 = serializers.CharField(max_length=255)
    password2 = serializers.CharField(max_length=255)

@api_view(['POST', ])
def user_registration(request):
    serializer = RegistrationFormSerilizer(request.data)
    form = forms.RegisterForm(serializer.data)

    if form.is_valid():
        user = form.save(commit=False)
        user.set_password(form.cleaned_data['password2'])
        user.is_active = True
        user.save()
        
        if request.data['user_type'] == 'student':
            # create user profile
            user_profile = UserProfile.profiles.create(
                user = user,
                mobile_contact = request.data['mobile_number'],
                profile_image = request.data['picture'],
                level = request.data['level'],
                course = request.data['course'],
                stream = request.data['stream'],
                slug = request.data['slug']
            )
            user_profile.save()

        return Response(status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)
    return Response(status=status.HTTP_200_OK)


@api_view(['POST', ])
def user_login(request):

    class LoginFormSerilizer(serializers.Serializer):
        email = serializers.EmailField()
        password = serializers.CharField(max_length=255)
    
    # user profile serializer
    class UserProfileSerializer(serializers.ModelSerializer):
        class Meta:
            model = UserProfile
            fields = '__all__'

            
    # user serializer
    class UserSerializer(serializers.ModelSerializer):
        userprofile = UserProfileSerializer()
        class Meta:
            model = CustomUser
            fields = '__all__'

    serializer = LoginFormSerilizer(request.data)
    form = forms.LoginForm(serializer.data)

    if form.is_valid():
        data = form.cleaned_data
        user = authenticate(username=data['email'], password=data['password'])

        

        if user is not None:

            # prepare user to be sent with the response
            user_data = CustomUser.users.get(email=data['email'])
            serializer = UserSerializer(user_data)
            if user.is_active:
                request.session['username'] = data['email']
                login(request, user)
                return Response(status=status.HTTP_200_OK, data=serializer.data )
            elif user.activation_key == 1:
                user.is_active = True
                user.activation_key += random.randint(2, 1000000)
                user.save()
                login(request, user)
                return Response(status=status.HTTP_200_OK, data=serializer.data)
            else:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    return Response(status=status.HTTP_406_NOT_ACCEPTABLE)


@api_view(['GET', ])
def user_logout(request):
    if request.user.is_authenticated:
        logout(request)
        return Response(status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
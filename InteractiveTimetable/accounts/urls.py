from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('login', views.user_login, name='login'),
    path('register/user', views.user_registration, name='registration'),
    path('logout', views.user_logout, name='logout'),
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
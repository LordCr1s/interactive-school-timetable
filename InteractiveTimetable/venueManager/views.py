from django.shortcuts import render
from rest_framework.decorators import api_view
from venueManager.models import Venue
from rest_framework.response import Response
from rest_framework import status
from venueManager.serializers import VenueSerializer

# retrieveing a list of all venues
@api_view(['GET', ])
def get_venue(request):
    
    try:
        venues = Venue.venues.all()
        serializer = VenueSerializer(venues, many=True)
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    except Venue.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)
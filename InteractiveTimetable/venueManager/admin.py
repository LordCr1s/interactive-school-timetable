from django.contrib import admin
from venueManager.models import Venue

@admin.register(Venue)
class VenueAdmin(admin.ModelAdmin):
    list_display = ('name', 'class_size', 'status', 'description', 'active_session')

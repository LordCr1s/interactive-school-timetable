from django.db import models


class Venue(models.Model):

    AVAILABLE = 'Available'
    UNAVAILABLE = 'Unavailable'

    VENUE_STATUS = [
        (AVAILABLE, 'available'),
        (UNAVAILABLE, 'unavailable')
    ]

    name = models.CharField(max_length=255, unique=True)
    class_size = models.IntegerField()
    status = models.CharField(max_length=255, choices=VENUE_STATUS, default=UNAVAILABLE)
    description = models.TextField()
    active_session = models.BooleanField(default=False)

    venues = models.Manager()

    class Meta:
        db_table = 'venue'
        verbose_name = 'Venues'
        verbose_name_plural = 'Venue'

    def __str__(self):
        return self.name
    

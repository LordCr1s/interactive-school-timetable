from django.urls import path
from venueManager import views

urlpatterns = [
    # get / fetch venues
    path('get/venues', views.get_venue),
]


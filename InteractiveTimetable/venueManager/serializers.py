from venueManager.models import Venue
from rest_framework import serializers
from sessionReschedule.serializers import SessionSerializer

class VenueSerializer(serializers.ModelSerializer):
    
    venue_names = SessionSerializer(many=True)
    class Meta:
        model = Venue
        fields = '__all__'

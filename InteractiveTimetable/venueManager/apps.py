from django.apps import AppConfig


class VenuemanagerConfig(AppConfig):
    name = 'venueManager'

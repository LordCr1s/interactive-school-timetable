import React from 'react';
import { Platform } from 'react-native';
import {
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import VenueScreen from '../screens/VenueScreen';
import Notification from '../screens/NotificationScreen';
import SettingsScreen from '../screens/SettingsScreen';


import { Feather } from '@expo/vector-icons'
import { heightPercentageToDP } from 'react-native-responsive-screen';


// Timetable manager 
const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Timetable',
  tabBarIcon: ({ focused }) => (
    <Feather name='calendar' size={17} color={focused? '#47bac1' : '#FFF'} />
  ),
  header : null
};


// Venue management = LinkScreen
const VenueStack = createStackNavigator({
  Links: VenueScreen,
});

VenueStack.navigationOptions = {
  tabBarLabel: 'Venue',
  tabBarIcon: ({ focused }) => (
    <Feather name='map-pin' size={17} color={focused? '#47bac1' : '#FFF'} />
  ),
};


// Notification = Notification
const NotificationStack = createStackNavigator({
  Notification: Notification,
});

NotificationStack.navigationOptions = {
  tabBarLabel: 'notification',
  tabBarIcon: ({ focused }) => (
    <Feather name='bell' size={17} color={focused? '#47bac1' : '#FFF'} />
  ),
};


// Settings
const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <Feather name='settings' size={17} color={focused? '#47bac1' : '#FFF'} />
  ),
};



export default createBottomTabNavigator({
  HomeStack,
  VenueStack,
  NotificationStack,
  SettingsStack
},
{
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      let IconComponent = Ionicons;
      let iconName;
      if (routeName === 'Home') {
        iconName = `ios-information-circle${focused ? '' : '-outline'}`;
        // Sometimes we want to add badges to some icons. 
        // You can check the implementation below.
        IconComponent = HomeIconWithBadge; 
      } else if (routeName === 'Settings') {
        iconName = `ios-options`;
      }

      // You can return any component that you like here!
      return <IconComponent name={iconName} size={25} color="#7952B3" />;
    },
  }),
  tabBarOptions: {
    activeTintColor: '#47bac1',
    inactiveTintColor: '#FFF',
    tabStyle: { backgroundColor: '#0a0a0a', height: heightPercentageToDP('6.5%'), paddingBottom: 13, paddingTop: 6 },
    style: { borderTopColor: '#0a0a0a', borderBottomColor: '#0a0a0a'}
  },
}
);

import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet,View  } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation'
import Auth from './screens/auth/auth'
import Login from './screens/auth/Login'
import Register from './screens/auth/Register'
import SingleVenue from './screens/Content/SingleVenue'

import { createStore, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';

import LoginReducer from './screens/Data/LoginReducer'
import AppNavigator from './navigation/AppNavigator';


const store = createStore(LoginReducer);

// crate application structure
const AppStack = createStackNavigator({ Home: AppNavigator, Venue: SingleVenue });
const AuthStack = createStackNavigator({auth: Auth, Login: Login, Register: Register });

AppStack.navigationOptions = {
  header: null
}

AppNavigator.navigationOptions = {
  header: null
}


AuthStack.navigationOptions = {
  header: null
}

const Routing = createAppContainer(createSwitchNavigator(
  {
      Auth: AuthStack,
      App: AppStack
  },
  {
      initialRouteName: 'Auth'
  }
))


export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <Routing />
        </View>
      </Provider>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/robot-dev.png'),
      require('./assets/images/robot-prod.png'),
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      'space-mono': require('./assets/fonts/VisbyRoundCF-Heavy.ttf'),
    }),
  ]);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0a0a0a',
  },
});

// export const ROOT_URL = "http://192.168.43.29:8000/api/"
export const ROOT_URL = "http://172.20.10.5:8001/api/"
export const ROOT_IMAGE_URL = "http://172.20.10.5:8001"

export const makeGetRequest = (request_url) => {
    // logging request activities
    console.log('making ge request to ' + ROOT_URL + request_url + '....')
    return fetch(ROOT_URL + request_url, {
        method : 'GET',
        headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        },
    }).then(response => {
      if (response.ok) {
        console.log('receiving data from ' + ROOT_URL + request_url + '....')
        return response.json()
      } else {
        return null
      }
    }).catch(error => console.log(error))
}

export const makePostRequest = (request_url, request_data) => {
    return fetch(ROOT_URL + request_url, {
        method : 'POST',
        headers : {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
        },
        body : JSON.stringify(request_data)
    }).then(response => {
      if (response.ok) {
        return response.json()
      } else {
        return null
      }
    }).catch(error => console.log(error))
}

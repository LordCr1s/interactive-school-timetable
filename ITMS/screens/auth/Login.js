import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native'
import { Feather, AntDesign } from '@expo/vector-icons'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen'
import { ROOT_URL, makePostRequest, makeGetRequest } from '../utils'
import { Snackbar } from 'react-native-paper'
import { storeUserInfo } from '../Data/LoginReducer'
import { connect } from 'react-redux'



class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
        is_user_validated: true,
        user_input : {
            email : '',
            password  : '',
        }
    }
  }

  updateUserEmail = (text) => {
    let user = {...this.state.user_input}
    user.email = text
    this.setState({
      user_input: user
    })
  }

  updateUserPassword = (text) => {
    let user = {...this.state.user_input}
    user.password = text
    this.setState({
      user_input: user
    })
  }



   user_login = async () => {
     const response = await makePostRequest('auth/login', this.state.user_input)
     await this.props.storeUserInfo(response)
     this.props.navigation.navigate('App')
   }

  static navigationOptions = {
    header: null,

  };


      render() {
        return (
            <View style={styles.container}>
                <View>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <View style={styles.back_button}>
                            <Text style={styles.button_text}>
                                <Feather
                                    name="arrow-left"
                                    color="rgba(255, 255, 255, .9)"
                                    size={34}
                                />
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.top_content}>
                    <Text style={styles.brand}>
                        Welcome
                    </Text>
                    <Text style={styles.brand}>
                        Back
                    </Text>
                </View>
                <View style={styles.middle_content}>
                    <TextInput
                        style={styles.inputs}
                        placeholder="enter Valid Email"
                        keyboardType={'email-address'}
                        onChangeText={(text) => this.updateUserEmail(text)}
                        placeholderTextColor="rgba(255, 255, 255, .5)"
                    />
                    <TextInput
                        style={styles.inputs}
                        placeholder="password"
                        secureTextEntry={true}
                        onChangeText={(text) => this.updateUserPassword(text)}
                        placeholderTextColor="rgba(255, 255, 255, .5)"
                    />
                </View>
                <View style={styles.bottom_content}>
                    <Text style={[styles.button_text, {marginLeft: widthPercentageToDP('40%')}]}>
                        Login
                    </Text>
                    <TouchableOpacity onPress={() => this.user_login()}>
                        <View style={styles.login_button}>
                            <Text style={styles.button_text}>
                                <Feather
                                    name="arrow-right"
                                    color="rgba(255, 255, 255, .9)"
                                    size={34}
                                    style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                                />
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Text style={styles.text}>
                    @DIT 2019, Inc
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#0a0a0a",
        flex: 1,
    },
    top_content: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    middle_content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    bottom_content: {
        flex: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    logo: {
        width: widthPercentageToDP('25%'),
        height: heightPercentageToDP('15%')
    },
    brand : {
        color: '#FFF',
        fontFamily: 'space-mono',
        fontSize: heightPercentageToDP('8%'),
        marginBottom: heightPercentageToDP('3%'),
        textAlign: 'left',
        marginLeft: widthPercentageToDP('13%'),
    },
    login_button : {
        backgroundColor: '#7952B3',
        padding: 25,
        width: widthPercentageToDP('19%'),
        borderRadius: 300,
        marginLeft: widthPercentageToDP('7%')
    },
    text: {
        color: '#FFF',
        textAlign: 'center',
        fontFamily: 'space-mono',
        bottom: heightPercentageToDP('2.4%')
    },
    button_text: {
        color: '#FFF',
        textAlign: 'center',
        fontFamily: 'space-mono',
        fontSize: heightPercentageToDP('3.5%'),
    },
    inputs: {
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, .5)',
        color: '#FFF',
        height: heightPercentageToDP('5.7%'),
        width: widthPercentageToDP('77%'),
        paddingHorizontal: widthPercentageToDP("6%"),
    },
    back_button : {
        backgroundColor: 'transparent',
        padding: 29,
        width: widthPercentageToDP('20%'),
        borderRadius: 300,
        marginLeft: widthPercentageToDP('1%'),
        marginTop: widthPercentageToDP('4%'),
    },
})


const mapStateToProps = state => {
  let user = state.user
  return {
    user: user
  };
};

const mapDispatchToProps = {
  storeUserInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

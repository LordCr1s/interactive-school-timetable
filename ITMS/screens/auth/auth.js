import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Feather } from '@expo/vector-icons'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen'

export default class Auth extends Component {

    static navigationOptions = {
        header: null,
      };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.top_content}>
                    <Text style={styles.brand}>
                        Iteractive Timetable
                    </Text>
                    <Image
                    source={require('../../assets/images/calendar.png')}
                    style={styles.logo}
                    />
                </View>
                <View style={styles.bottom_content}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')}>
                        <View style={styles.login_button}>
                            <Text style={styles.button_text}>
                                Login
                            </Text>
                </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Register')}>
                        <View style={styles.signup_button}>
                            <Text style={styles.button_text}>
                                Sign up
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Text style={styles.text}>
                    @DIT 2019, Inc
                </Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#0a0a0a",
        flex: 1,
    },
    top_content: {
        flex: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bottom_content: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    }, 
    logo: {
        width: widthPercentageToDP('26%'),
        height: heightPercentageToDP('13%')
    },
    brand : {
        color: '#FFF',
        fontFamily: 'space-mono',
        fontSize: heightPercentageToDP('4.5%'),
        marginBottom: heightPercentageToDP('3%'),
    },
    login_button : {
        backgroundColor: '#7952B3',
        padding: 13,
        width: widthPercentageToDP('28%'),
        borderRadius: 20,
    },
    signup_button : {
        backgroundColor: '#47bac1',
        padding: 13,
        width: widthPercentageToDP('28%'),
        borderRadius: 20,
    },
    text: {
        color: '#FFF',
        textAlign: 'center',
        fontFamily: 'space-mono',
        bottom: heightPercentageToDP('2.4%')
    },
    button_text: {
        color: '#FFF', 
        textAlign: 'center', 
        fontFamily: 'space-mono'
    }
})
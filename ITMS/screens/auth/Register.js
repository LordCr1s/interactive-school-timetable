import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native'
import { Feather, AntDesign } from '@expo/vector-icons'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen'
import { ROOT_URL, makePostRequest, makeGetRequest } from '../utils'
import Login from './Login'
import { Snackbar } from 'react-native-paper'



export default class Register extends Component {

    constructor(props) {
        super(props)
        this.state = {
            is_user_validated: false,
            is_snackbar_visible: false,

            user_input : {
                reg_number : '',
                email : '',
                mobile_contact : '',
                password1 : '',
                password2 : '',
            },

            user : {
                email:'',
                mobile_number:'',
                password1:'',
                password2:'',
                first_name:'',
                last_name:'',
                registration_number:'',
                user_type:'',
                level: '',
                slug: '',
                course: '',
                stream: '',
                picture: ''
            }
        }
    }

    update_reg_number = (text) => {
        // clonning user input object so that user_input_Clone is same as user_input
        let user_input_clone = {...this.state.user_input}
        // changing the value of the reg number on user input clone
        user_input_clone.reg_number = text
        // update the user input by making it equal to user input clone, i.e giving new values
        this.setState({user_input: user_input_clone})
        console.log(text);
    }

    update_email = (text) => {
        let user_email = {...this.state.user_input}
        user_email.email = text
        this.setState({user_input:  user_email})
        console.log(text);

    }

    update_mobile_contact = (text) => {
        let mobile_contact = {...this.state.user_input}
        mobile_contact.mobile_contact = text
        this.setState({user_input: mobile_contact})
        console.log(text);

    }

    update_password1 = (text) => {
        let user_pass1 = {...this.state.user_input}
        user_pass1.password1 = text
        this.setState({user_input: user_pass1})
        console.log(text);

    }

    update_password2 = (text) => {
        let user_pass2 = {...this.state.user_input}
        user_pass2.password2 = text
        this.setState({user_input: user_pass2})
        console.log(text);

    }

    // Validation user
    validate_user = async () => {
        const response = await makeGetRequest('osim/validate/user/'+ this.state.user_input.reg_number)
        console.log(response)
        if (response) {

            let user = {...this.state.user}

            user.first_name = response.first_name
            user.last_name = response.last_name
            user.email = this.state.user_input.email
            user.mobile_number = this.state.user_input.mobile_contact,
            user.password1 = this.state.user_input.password1
            user.password2 = this.state.user_input.password2
            user.registration_number = response.registration_number
            user.user_type = response.user_type
            user.picture = response.picture

            // add user information for students only
            if (response.user_type == 'student') {
              user.level = response.information.level
              user.slug = response.information.identity
              user.course = response.information.course
              user.stream = response.information.stream
            }

            this.setState({
                is_user_validated: true,
                user : user
            })

        } else {
           this.setState({ is_snackbar_visible : true, snackbar_message: 'user is invalid' })
        }
    }

    register_user  = async () => {
            let user = {...this.state.user}

            user.email = this.state.user_input.email
            user.mobile_number = this.state.user_input.mobile_contact,
            user.password1 = this.state.user_input.password1
            user.password2 = this.state.user_input.password2

            this.setState({
                user : user
            })

            // check for password validity before registering the user
            if (user.password1 === user.password2) {
              const response = await makePostRequest('auth/register/user', this.state.user)
              if (response != null) {
                this.props.navigation.navigate('Apps')
              }

            } else { this.setState({ is_snackbar_visible: true, snackbar_message: 'user can not be regisered' }) }



    }



    static navigationOptions = {
        header: null,

      };

    render() {

      const { is_snackbar_visible } = this.state

        return (
            <View style={styles.container}>
                <View>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <View style={styles.back_button}>
                            <Text style={styles.button_text}>
                                <Feather
                                    name="arrow-left"
                                    color="rgba(255, 255, 255, .9)"
                                    size={34}
                                />
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.top_content}>
                    <Text style={styles.brand}>
                        Sign Up
                    </Text>
                </View>
                {
                    this.state.is_user_validated ?

                    <KeyboardAvoidingView
                        style={styles.middle_content}
                        behavior="padding"
                        contentContainerStyle={styles.middle_content_keyboard}>
                        <TextInput
                            style={styles.inputs}
                            placeholder="Enter valid Email"
                            keyboardType={'email-address'}
                            onChangeText={(text) => this.update_email(text)}
                            placeholderTextColor="rgba(255, 255, 255, .5)"
                        />
                        <TextInput
                            style={styles.inputs}
                            placeholder="mobile contact"
                            secureTextEntry={false}
                            onChangeText={(text) => this.update_mobile_contact(text)}
                            placeholderTextColor="rgba(255, 255, 255, .5)"
                        />
                        <TextInput
                            style={styles.inputs}
                            placeholder="Enter password"
                            secureTextEntry={true}
                            onChangeText={(text) => this.update_password1(text)}
                            placeholderTextColor="rgba(255, 255, 255, .5)"
                        />
                        <TextInput
                            style={styles.inputs}
                            placeholder="Confirm password"
                            secureTextEntry={true}
                            onChangeText={(text) => this.update_password2(text)}
                            placeholderTextColor="rgba(255, 255, 255, .5)"
                        />
                    </KeyboardAvoidingView>
                    :
                    <KeyboardAvoidingView
                        style={styles.middle_content}
                        behavior="padding"
                        contentContainerStyle={styles.middle_content_keyboard}>
                        <TextInput
                            style={styles.inputs}
                            placeholder="Registration Number"
                            keyboardType={'email-address'}
                            onChangeText={(text) => this.update_reg_number(text)}
                            placeholderTextColor="rgba(255, 255, 255, .5)"
                        />
                    </KeyboardAvoidingView>

                }
                <View style={styles.bottom_content}>
                    <Text style={[styles.button_text, {marginLeft: widthPercentageToDP('40%')}]}>
                        Sign up
                    </Text>
                    {
                        this.state.is_user_validated ?

                        <TouchableOpacity onPress={() => this.register_user()}>
                            <View style={styles.login_button}>
                                <Text style={styles.button_text}>
                                    <Feather
                                        name="arrow-right"
                                        color="rgba(255, 255, 255, .9)"
                                        size={34}
                                        style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                                    />
                                </Text>
                            </View>
                        </TouchableOpacity>

                        :
                        <TouchableOpacity onPress={() => this.validate_user()}>
                            <View style={styles.login_button}>
                                <Text style={styles.button_text}>
                                    <Feather
                                        name="arrow-right"
                                        color="rgba(255, 255, 255, .9)"
                                        size={34}
                                        style={{ backgroundColor: 'transparent', marginLeft: 10 }}
                                    />
                                </Text>
                            </View>
                        </TouchableOpacity>
                    }

                </View>
                <Text style={styles.text}>
                    @DIT 2019, Inc
                </Text>

                <Snackbar
                    visible={this.state.is_snackbar_visible}
                    onDismiss={() => this.setState({ is_snackbar_visible: false })}>
                    <Text styles={{ flex: 1, flexDirection: 'row'}}>
                        <Text styles={{flex: 1, backgroundColor: 'red'}}>

                        </Text>
                        <Text styles={{flex: 10}}>
                            <Text>
                                { this.state.snackbar_message }
                            </Text>
                        </Text>
                    </Text>
                </Snackbar>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#0a0a0a",
        flex: 1,
    },
    top_content: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    middle_content: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    middle_content_keyboard: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'space-evenly',
        position: 'absolute',
        bottom: heightPercentageToDP('50%'),
    },
    bottom_content: {
        flex: 2,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    logo: {
        width: widthPercentageToDP('25%'),
        height: heightPercentageToDP('15%')
    },
    brand : {
        color: '#FFF',
        fontFamily: 'space-mono',
        fontSize: heightPercentageToDP('8%'),
        marginBottom: heightPercentageToDP('3%'),
        textAlign: 'left',
        marginLeft: widthPercentageToDP('13%'),
    },
    login_button : {
        backgroundColor: '#47bac1',
        padding: 25,
        width: widthPercentageToDP('19%'),
        borderRadius: 300,
        marginLeft: widthPercentageToDP('7%')
    },
    text: {
        color: '#FFF',
        textAlign: 'center',
        fontFamily: 'space-mono',
        bottom: heightPercentageToDP('2.4%')
    },
    button_text: {
        color: '#FFF',
        textAlign: 'center',
        fontFamily: 'space-mono',
        fontSize: heightPercentageToDP('3.5%'),
    },
    inputs: {
        borderRadius: 5,
        flex: 0,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, .5)',
        color: '#FFF',
        height: heightPercentageToDP('5.7%'),
        width: widthPercentageToDP('77%'),
        paddingHorizontal: widthPercentageToDP("6%"),
    },
    back_button : {
        backgroundColor: 'transparent',
        padding: 29,
        width: widthPercentageToDP('20%'),
        borderRadius: 300,
        marginLeft: widthPercentageToDP('1%'),
        marginTop: widthPercentageToDP('4%'),
    },
})

import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen'
import Timetable  from './Content/Timetable'
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { Avatar } from 'react-native-paper'
import { connect } from 'react-redux'
import { makeGetRequest } from './utils.js'
import { storeUserInfo } from './Data/LoginReducer'
import Venue from './Content/Venues'
import { ROOT_IMAGE_URL } from './utils'
import { FlatList } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons'

const TopSlider = createAppContainer(
  createSwitchNavigator({
    Main: Timetable,
  })
);

class HomeScreen extends Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props){
    super(props)
    this.state = {
      timetable: [],
      refreshing: false,
    }
  }

  getNotifications = async () => {
    if (this.props.user.user_type == 'student') {
      const response = await makeGetRequest('session/get/timetable?course='+
      this.props.user.userprofile.course +'&level='+ this.props.user.userprofile.level )

      this.setState({
        timetable : response
       })
    }
  }

  async componentDidMount() {

    this.getNotifications()
    
    
  }

  handleRefresh = () => {
    this.setState({
      refreshing: true
    }, () => this.getNotifications())
  }



  render() {

    const user = this.props.user
    return (
      <View style={styles.container}>
        <View style={styles.top_section}>
          <Text style={styles.header_text}>
            { user.first_name + " " + user.last_name}
          </Text>
          <Text style={styles.section_description}>
              { user.registration_number }
          </Text>
        </View>
        <View style={styles.main_section}>
          <Text style={[styles.small_header_text, {marginLeft: 20}]}>
            Notifications
          </Text>
          
          <FlatList 
            refreshing = {this.state.refreshing}
            onRefresh = { this.handleRefresh}
            data={this.state.timetable.sessions} renderItem={({item}) => 


              item.notifications.map(notification => (
                <View style={styles.session} key={notification.id}>
                    <Text style={styles.session_module}>
                    <Feather name='bell' size={15} color="#fcc100" />  Session Reschedule
                    </Text>
                    <Text style={styles.session_module}>
                    {notification.content}
                    </Text>
                    <Text style={[styles.session_module, {textAlign: 'right'}]}>
                    <Feather name='clock' size={15} color="#fcc100" />  {notification.time_sent}
                  </Text>
              </View>
              ))


            }/>
        </View>
        <View style={styles.header}>
        <Avatar.Image size={80} source={
                ( user.user_type == 'student') ?
                {uri: ROOT_IMAGE_URL + user.userprofile.profile_image } :
                 require('../assets/images/logo.png') }/>

        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    backgroundColor: '#47bac1',
    flex: 1
  },
  top_section: {
    flex: 1,
    backgroundColor: '#47bac1'
  },
  top_section_lecturer: {
    flex: 1,
    backgroundColor: '#7952B3'
  },
  header: {
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: '#47bac1',
    borderRadius: 300,
    padding: 4,
    marginLeft: widthPercentageToDP('68%'),
    marginTop: heightPercentageToDP('4%')
  },
  main_section: {
    flex: 7,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    backgroundColor: '#151414',
    paddingTop: heightPercentageToDP('1.8%')
  },
  header_text : {
    fontFamily: 'space-mono',
    color: '#FFF',
    fontSize: heightPercentageToDP('2.8%'),
    marginTop: heightPercentageToDP('4%'),
    marginLeft: widthPercentageToDP("5%"),
  },
  small_header_text : {
    fontFamily: 'space-mono',
    color: '#FFF',
    fontSize: heightPercentageToDP('4%'),
    marginBottom: heightPercentageToDP('3%')
  },
  section_description: {
    fontFamily: 'space-mono',
    color: '#FFF',
    marginLeft: widthPercentageToDP('5%'),
    fontSize: heightPercentageToDP('2.2%'),
    marginTop: 5
  },
  session: {
    width: widthPercentageToDP('90%'),
    marginLeft: widthPercentageToDP('5%'),
    backgroundColor: '#0a0a0a',
    marginTop: 10,
    padding: 15,
    borderRadius: 10,
  },
  session_module: {
      color: '#FFF',
      fontFamily: 'space-mono',
      fontSize: heightPercentageToDP('2%'),
      marginBottom: 5,
  },
  })

const mapStateToProps = state => {
  let user = state.user
  return {
    user: user
  };
};

const mapDispatchToProps = {
  storeUserInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

export const USER_HAS_LOGGED_IN = 'user:login';

const initialState = {
  user : {}
}

export default LoginReducer = (state = initialState, {action, payload}) => {
  switch (action) {
    case USER_HAS_LOGGED_IN:
      return { ...state, user: payload };
    default:
      return { ...state, user: payload };
  }
}

export function storeUserInfo(user) {
  return {
    type: USER_HAS_LOGGED_IN,
    payload: user
  };
}


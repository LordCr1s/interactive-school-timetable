export const SHOW_TIME_TABLE = 'user:timetable';

const initialState = {
  timetable : {}
}

export default TimetableReducer = (state = initialState, {action, payload}) => {
  console.log(payload)
  switch (action) {
    case SHOW_TIME_TABLE:
      return { ...state, user: payload };
    default:
      return { ...state, timetable: timetable };
  }
}

export function storeTimetable(timetable) {
  return {
    type: SHOW_TIME_TABLE,
    payload: timetable
  };
}

import { Text, View, StyleSheet, ScrollView } from 'react-native'
import React, { Component } from 'react'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import { Feather } from '@expo/vector-icons'
import { storeUserInfo } from '../Data/LoginReducer'
import { connect } from 'react-redux'
import { makeGetRequest } from '../utils'

class Monday extends Component {

    static navigationOptions = {
        header: null,
      };

      constructor(props){
        super(props)
        this.state = {
          timetable: {},
          isFetching: true,
          lecturers_session: []
        }
      }

      async componentDidMount() {
        if (this.props.user.user_type == 'student') {
            const response = await makeGetRequest('session/get/timetable?course='+
            this.props.user.userprofile.course +'&level='+ this.props.user.userprofile.level )
      
             if (response != null ) {
              this.setState({
                timetable : response,
                isFetching: false
              })
             }
        } else if (this.props.user.user_type == 'lecturer'){
          const response = await makeGetRequest('session/get/sessions/by/lecturer/'+ this.props.user.id )
              
             if (response != null ) {
               console.log('i am here too')
              this.setState({
                lecturers_session : response,
                isFetching: false
              })
             }
             
        }
      }



    render() {
      let sessions = []
      if (this.props.user.user_type == "student") {
          sessions = this.state.timetable.sessions
      } else {
        sessions = this.state.lecturers_session
      }
      return (
        <ScrollView contentContainerStyle={styles.contentContainer}>
           
            <View style={styles.main_session_container}>
              {
                this.state.isFetching ? 
                    <Text style={{fontFamily: 'space-mono', color: '#FFF', fontSize: heightPercentageToDP('2.4%')}}>
                      session loading....
                    </Text>
                :

                
                sessions.map(session => (

                  (session.day_of_the_week == "monday") ?
                  <View style={styles.session} key={session.id}>
                      <Text style={styles.session_module}>
                      <Feather name='edit-2' size={15} color="#fcc100" />  {session.module.code + " " + session.module.name}
                      </Text>
                      <Text style={styles.session_module}>
                      <Feather name='award' size={15} color="#fcc100" />  {session.lecturer.first_name + " " + session.lecturer.last_name}
                      </Text>
                      <Text style={styles.session_module}>
                      <Feather name='map-pin' size={15} color="#fcc100" />  {session.venue.name}
                      </Text>
                      <Text style={styles.session_module}>
                      <Feather name='clock' size={15} color="#fcc100" />  {session.start_time}
                      - <Feather name='loader' size={12} color="#FFF" /> - {session.end_time}
                      </Text>
                  </View> : null
                )) 
              }
                
            </View>
        </ScrollView>

      )
    }
  }

  const styles = StyleSheet.create({
    main_session_container : {
      backgroundColor: '#151414',
      flex: 1,
      alignItems: 'center',
      paddingTop: 10,
    },
    session: {
        width: widthPercentageToDP('90%'),
        backgroundColor: '#0a0a0a',
        marginTop: 10,
        padding: 15,
        borderRadius: 10,
    },
    session_module: {
        color: '#FFF',
        fontFamily: 'space-mono',
        fontSize: heightPercentageToDP('2%'),
        marginBottom: 5,
    },
    contentContainer:{
        backgroundColor: '#151414',
        flex: 1,
    }
  })

const mapStateToProps = state => {
  let user = state.user
  return {
    user: user
  };
};

const mapDispatchToProps = {
  storeUserInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(Monday);
import { Text, View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import React, { Component } from 'react'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import { Feather } from '@expo/vector-icons'
import { storeUserInfo } from '../Data/LoginReducer'
import { connect } from 'react-redux'
import { makeGetRequest } from '../utils'

class Monday extends Component {

    static navigationOptions = {
        header: null,
      };

      constructor(props){
        super(props)
        this.state = {
          timetable: [],
          isFetching: true,
        }
      }

      async componentDidMount() {
        
            const response = await makeGetRequest('venue/get/venues')
             if (response != null ) {
              this.setState({
                timetable : response,
                isFetching: false
              })
             
        }
      }



    render() {
      return (
        <ScrollView>
            <View style={styles.main_session_container}>
              {
                this.state.isFetching ? 
                    <Text style={{fontFamily: 'space-mono', color: '#FFF', fontSize: heightPercentageToDP('2.4%')}}>
                      session loading....
                    </Text>
                :

                
                this.state.timetable.map(venue => (

                <TouchableOpacity onPress={() => this.props.navigate('Venue', {venue_details: venue})}>
                  <View style={styles.venue} key={venue.id}>
                      <Text style={styles.venue_module}>
                      <Feather name='map-pin' size={15} color="#47bac1" />  {venue.name}
                      </Text>
                      <Text style={styles.venue_module}>
                      <Feather name='slack' size={15} color="#47bac1" />  {"class size of " + venue.class_size}
                      </Text>
                      <Text style={styles.venue_module}>
                        {venue.description}
                      </Text>
                      <Text style={styles.venue_module}>
                     
                      <Feather name='loader' size={12} color="#FFF" />  {venue.status} | { "active session : " + venue.active_session}
                      </Text>
                  </View>
                  </TouchableOpacity>
                )) 
              }
                
            </View>
        </ScrollView>

      )
    }
  }

  const styles = StyleSheet.create({
    main_session_container : {
      backgroundColor: '#151414',
      flex: 1,
      alignItems: 'center',
      paddingTop: 15,
      paddingBottom: heightPercentageToDP('20%')
    },
    venue: {
        width: widthPercentageToDP('90%'),
        backgroundColor: '#0a0a0a',
        marginTop: 10,
        padding: 15,
        borderRadius: 10,
    },
    venue_module: {
        color: '#FFF',
        fontFamily: 'space-mono',
        fontSize: heightPercentageToDP('2%'),
        marginBottom: 5,
    },
    contentContainer:{
        backgroundColor: '#151414',
        flex: 1,
    }
  })

const mapStateToProps = state => {
  let user = state.user
  return {
    user: user
  };
};

const mapDispatchToProps = {
  storeUserInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(Monday);
import { createMaterialTopTabNavigator, createStackNavigator } from 'react-navigation'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen'
import { Text, View, StyleSheet } from 'react-native'
import React, { Component } from 'react'

import Monday from '../Days/Monday'
import Tuesday from '../Days/Tuesday'
import Wednesday from '../Days/Wednesday'
import Thursday from '../Days/Thursday'
import Friday from '../Days/Friday'


// Monday TopTab
const MondayStack = createStackNavigator({
    monday: Monday,
  });
  
  MondayStack.navigationOptions = {
    tabBarLabel: <Text style={{ 
                    color: '#FFF', 
                    fontFamily: 'space-mono', 
                    fontSize: heightPercentageToDP('2%')}}>
                      Monday
                </Text>,
  };
  
  // Tuesday TopTab
  const TuesdayStack = createStackNavigator({
    tuesday: Tuesday,
  });
  
  TuesdayStack.navigationOptions = {
    tabBarLabel: <Text style={{ 
                      color: '#FFF', 
                      fontFamily: 'space-mono', 
                      fontSize: heightPercentageToDP('2%')}}>
                        Tuesday
                  </Text>,
      };


// Wednesday TopTab
  const WednesdayStack = createStackNavigator({
    wednesday: Wednesday,
  });
  
  WednesdayStack.navigationOptions = {
    tabBarLabel: <Text style={{ 
                    color: '#FFF', 
                    fontFamily: 'space-mono', 
                    fontSize: heightPercentageToDP('2%')}}>
                      Wednesday
                </Text>,
  };


// Thursday TopTab
  const ThursdayStack = createStackNavigator({
    thursday: Thursday,
  });
  
  ThursdayStack.navigationOptions = {
    tabBarLabel: <Text style={{ 
                    color: '#FFF', 
                    fontFamily: 'space-mono', 
                    fontSize: heightPercentageToDP('2%')}}>
                      Thursday
                </Text>,
  };

// Friday TopTab
  
  const FridayStack = createStackNavigator({
    firday: Friday,
  });
  
  FridayStack.navigationOptions = {
    tabBarLabel: <Text style={{ 
                    color: '#FFF', 
                    fontFamily: 'space-mono', 
                    fontSize: heightPercentageToDP('2%')}}>
                      Friday
                </Text>,
  };

  


export default createMaterialTopTabNavigator({
    MondayStack,
    TuesdayStack,
    WednesdayStack,
    ThursdayStack,
    FridayStack
  }, 
  
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === 'monday') {
          iconName = `ios-information-circle${focused ? '' : '-outline'}`;
          // Sometimes we want to add badges to some icons. 
          // You can check the implementation below.
          IconComponent = HomeIconWithBadge; 
        } else if (routeName === 'Settings') {
          iconName = `ios-options`;
        }
  
        // You can return any component that you like here!
        return <IconComponent name={iconName} size={25} color="#7952B3" />;
      },
    }),
    tabBarOptions: {
      scrollEnabled: true,
      activeTintColor: '#47bac1',
      inactiveTintColor: '#FFF',
      indicatorStyle : { 
          top: heightPercentageToDP('4%'), 
          backgroundColor: '#47bac1', 
          height: heightPercentageToDP('.46%'),
          borderRadius: 10
        },
      tabStyle: { backgroundColor: '#0a0a0a' },
      style: { backgroundColor: '#0a0a0a' },
    },
  }
  );


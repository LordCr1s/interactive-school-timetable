import React, { Component } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen'
import Timetable  from './Timetable'
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { Avatar } from 'react-native-paper'
import { connect } from 'react-redux'
import { makeGetRequest } from '../utils.js'
import { storeUserInfo } from '../Data/LoginReducer'
import Venue from './Venues'
import { ROOT_IMAGE_URL, makePostRequest } from '../utils'
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons'
import {Modal, Picker, TextInput} from 'react-native'

const TopSlider = createAppContainer(
  createSwitchNavigator({
    Main: Timetable,
  })
);

class HomeScreen extends Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props){
    super(props)
    this.state = {
      timetable: [],
      refreshing: false,
      venue: {},
      is_settingup_sessions: true,
      modalVisible: false,
      lecturers_session: [],
      selected_session: '',
      user_input : {
        day_of_the_week : '',
        end_time  : '',
        start_time: '',
        venue_id: '',
        module_id : '',
        reschedule_type: 'temporary',
        prev_day: ''
    }
    }
  }

  rescheduleSession = async (id) => {
    let user = {...this.state.user_input}
    user.venue_id = id
    this.setState({user_input: user}, async () => {
      const response = await makePostRequest('session/reschedule', this.state.user_input)
      if (response.ok) {
        alert('success')
      }
    } )
     
  }

  updateModule = (text) => {
    let user = {...this.state.user_input}
    user.module_id = text
    this.setState({
      user_input: user
    })
  }

  updatePre = (text) => {
    let user = {...this.state.user_input}
    user.prev_day = text
    this.setState({
      user_input: user
    })
  }

  updateDay = (text) => {
    let user = {...this.state.user_input}
    user.day_of_the_week = text
    this.setState({
      user_input: user
    })
  }

  updateEnd = (text) => {
    let user = {...this.state.user_input}
    user.end_time = text
    this.setState({
      user_input: user
    })
  }

  updateStart = (text) => {
    let user = {...this.state.user_input}
    user.start_time = text
    this.setState({
      user_input: user
    })
  }

  getNotifications = async () => {
    const response = await makeGetRequest('session/get/sessions/by/lecturer/'+ this.props.user.id )      
    if (response != null ) {
      this.setState({
        lecturers_session : response,
        isFetching: false
      })
    }
  }

  async componentDidMount() {
    this.getNotifications()
    this.setState({
        venue : this.props.navigation.getParam('venue_details'), 
        is_settingup_sessions: false
    })
    
    
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  handleRefresh = () => {
    this.setState({
      refreshing: true
    }, () => this.getNotifications())
  }

  renderSession = (session) => {
    return (
      <View style={styles.session} key={session.id}>
          <Text style={[styles.session_module, {fontSize: heightPercentageToDP('3'), color: '#47bac1'}]}>
            {session.day_of_the_week}
          </Text>
          <Text style={styles.session_module}>
          <Feather name='edit-2' size={15} color="#47bac1" />  {session.module.code + " " + session.module.name}
          </Text>
          <Text style={styles.session_module}>
          <Feather name='award' size={15} color="#47bac1" />  {session.lecturer.first_name + " " + session.lecturer.last_name}
          </Text>
          <Text style={styles.session_module}>
          <Feather name='map-pin' size={15} color="#47bac1" />  {session.venue.name}
          </Text>
          <Text style={styles.session_module}>
          <Feather name='clock' size={15} color="#47bac1" />  {session.start_time}
          - <Feather name='loader' size={12} color="#FFF" /> - {session.end_time}
          </Text>
      </View>
    )
  }



  render() {

    const user = this.props.user
    return (
      <View style={styles.container}>
        <View style={styles.top_section}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <View style={styles.back_button}>
                  <Text style={styles.button_text}>
                      <Feather
                          name="arrow-left"
                          color="47bac1"
                          size={34}
                      />
                  </Text>
              </View>
          </TouchableOpacity>
        </View>
        <View style={styles.main_section}>
          <View style={{backgroundColor: '#0a0a0a', width: widthPercentageToDP('100%')}}>
            <Text style={[styles.small_header_text, {marginLeft: 20,     marginBottom: heightPercentageToDP('1%')}]}>
              {this.state.venue.name} 
            </Text>
            <Text style={styles.venue_module}>
            <Feather name='slack' size={15} color="#47bac1" />  {"class size of " + this.state.venue.class_size}
            </Text>
            <Text style={styles.venue_module}>
              {this.state.venue.description}
            </Text>
            <Text style={styles.venue_module}>
            <Feather name='loader' size={12} color="#FFF" />  {this.state.venue.status} | { "active session : " + this.state.venue.active_session}
            </Text>
            
          </View>
          <ScrollView>
          {this.state.is_settingup_sessions ? 
          
            <Text style={{fontFamily: 'space-mono', color: '#FFF', fontSize: heightPercentageToDP('2.4%')}}>
              session loading....
            </Text>

          : this.state.venue.venue_names.map( session => (this.renderSession(session)))}
          </ScrollView>
        </View>
        <View style={styles.header}>
        <Avatar.Image size={60} source={
                ( user.user_type == 'student') ?
                {uri: ROOT_IMAGE_URL + user.userprofile.profile_image } :
                 require('../../assets/images/logo.png') }/>

          { (user.user_type == 'student')?  null :
          
          <View style={{ flexDirection: 'row', marginTop: heightPercentageToDP('4%')}}>
                <TouchableOpacity
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                  <View style={{backgroundColor: '#7952B3', padding: 5, borderRadius: 3}}>
                    <Text style={{color: '#FFF', fontFamily: 'space-mono'}}>
                      reschedule session
                    </Text>
                  </View>
                </TouchableOpacity>

                <Modal
                  animationType="slide"
                  transparent={true}
                  visible={this.state.modalVisible}
                  onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                  }}>
                  <View style={styles.modal}>
                  <TextInput
                        style={styles.inputs}
                        placeholder="enter module code"
                        keyboardType={'email-address'}
                        onChangeText={(text) => this.updateModule(text)}
                        placeholderTextColor="rgba(255, 255, 255, .5)"
                    />
                    <TextInput
                        style={styles.inputs}
                        placeholder="enter day of the week"
                        keyboardType={'email-address'}
                        onChangeText={(text) => this.updateDay(text)}
                        placeholderTextColor="rgba(255, 255, 255, .5)"
                    />
                    <TextInput
                        style={styles.inputs}
                        placeholder="session was previously at"
                        keyboardType={'email-address'}
                        onChangeText={(text) => this.updatePre(text)}
                        placeholderTextColor="rgba(255, 255, 255, .5)"
                    />
                    <TextInput
                        style={styles.inputs}
                        placeholder="start time in 00:00 24hrs"
                        onChangeText={(text) => this.updateStart(text)}
                        placeholderTextColor="rgba(255, 255, 255, .5)"
                    />
                    <TextInput
                        style={styles.inputs}
                        placeholder="end time in 00:00 24hrs"
                        onChangeText={(text) => this.updateEnd(text)}
                        placeholderTextColor="rgba(255, 255, 255, .5)"
                    />

                <TouchableOpacity
                    onPress={() => {
                      this.setModalVisible(!this.state.modalVisible);
                    }}>
                      <View style={{backgroundColor: '#7952B3', padding: 5, borderRadius: 3}}>
                        <Text style={{color: '#FFF', fontFamily: 'space-mono'}}>
                          cancel
                        </Text>
                      </View>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => {
                      this.rescheduleSession(this.state.venue.id)
                    }}>
                      <View style={{backgroundColor: '#7952B3', padding: 5, borderRadius: 3}}>
                        <Text style={{color: '#FFF', fontFamily: 'space-mono'}}>
                          reschedule session
                        </Text>
                      </View>
                </TouchableOpacity>

                  </View>

                  

                </Modal>

            </View>            
            }
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    backgroundColor: '#47bac1',
    flex: 1
  },
  modal : {
    backgroundColor: '#0a0a0a',
    width: widthPercentageToDP('80%'),
    position: 'absolute',
    height: heightPercentageToDP('50%'),
    padding: 20,
    marginLeft: widthPercentageToDP('10%'),
    marginTop: heightPercentageToDP('25%')
  },
  top_section: {
    flex: 1,
    backgroundColor: '#0a0a0a',
    alignItems: 'flex-start'
  },
  top_section_lecturer: {
    flex: 1,
    backgroundColor: '#7952B3'
  },
  header: {
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
    borderRadius: 300,
    padding: 4,
    marginLeft: widthPercentageToDP('68%'),
    marginTop: heightPercentageToDP('4%')
  },
  main_section: {
    flex: 10,
    backgroundColor: '#151414',
    paddingTop: 0
  },
  header_text : {
    fontFamily: 'space-mono',
    color: '#FFF',
    fontSize: heightPercentageToDP('2.8%'),
    marginTop: heightPercentageToDP('4%'),
    marginLeft: widthPercentageToDP("5%"),
  },
  small_header_text : {
    fontFamily: 'space-mono',
    color: '#FFF',
    fontSize: heightPercentageToDP('4%'),
  },
  section_description: {
    fontFamily: 'space-mono',
    color: '#FFF',
    marginLeft: widthPercentageToDP('5%'),
    fontSize: heightPercentageToDP('2.2%'),
    marginTop: 5
  },
  session: {
    width: widthPercentageToDP('90%'),
    marginLeft: widthPercentageToDP('5%'),
    backgroundColor: '#0a0a0a',
    marginTop: 10,
    padding: 15,
    borderRadius: 10,
  },
  session_module: {
      color: '#FFF',
      fontFamily: 'space-mono',
      fontSize: heightPercentageToDP('2%'),
      marginBottom: 5,
  },
  button_text: {
    color: '#FFF',
    textAlign: 'center',
    fontFamily: 'space-mono',
    fontSize: heightPercentageToDP('3.5%'),
  },
  inputs: {
      borderRadius: 5,
      flex: 0,
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: 'rgba(0, 0, 0, .5)',
      color: '#FFF',
      height: heightPercentageToDP('5.7%'),
      width: widthPercentageToDP('77%'),
      paddingHorizontal: widthPercentageToDP("6%"),
  },
  back_button : {
      backgroundColor: 'transparent',
      borderRadius: 300,
      marginTop: widthPercentageToDP('6.5%'),
      marginLeft: widthPercentageToDP('2%')
  },
  venue_module: {
    color: '#FFF',
    fontFamily: 'space-mono',
    fontSize: heightPercentageToDP('2%'),
    marginBottom: 5,
    marginLeft: 20
},
  })

const mapStateToProps = state => {
  let user = state.user
  return {
    user: user
  };
};

const mapDispatchToProps = {
  storeUserInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

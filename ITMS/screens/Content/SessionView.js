import { Text, View, StyleSheet } from 'react-native'
import React, { Component } from 'react'
import { widthPercentageToDP, heightPercentageToDP } from 'react-native-responsive-screen';
import { Feather } from '@expo/vector-icons'

export default class SessionView extends Component {

    static navigationOptions = {
        header: null,
      };

    render() {
      return (
        <View style={styles.main_session_container}>
            <View style={styles.session}>
                <Text style={styles.session_module}>
                  <Feather name='edit-2' size={15} color="#fcc100" />  Module Name 
                </Text>
                <Text style={styles.session_module}>
                  <Feather name='award' size={15} color="#fcc100" />  Lecturer Name
                </Text>
                <Text style={styles.session_module}>
                  <Feather name='map-pin' size={15} color="#fcc100" />  Venue Name
                </Text>
                <Text style={styles.session_module}>
                  <Feather name='clock' size={15} color="#fcc100" />  Starting Time 
                  - <Feather name='loader' size={12} color="#FFF" /> - Ending Time
                </Text>
            </View>
        </View>
      )
    }
  }
  
  const styles = StyleSheet.create({
    main_session_container : {
      backgroundColor: '#151414',
      flex: 1,
      alignItems: 'center',
      paddingTop: 10,
    },
    session: {
        width: widthPercentageToDP('90%'),
        backgroundColor: '#0a0a0a',
        marginTop: 10,
        padding: 15,
        borderRadius: 10,
    },
    session_module: {
        color: '#FFF',
        fontFamily: 'space-mono',
        fontSize: heightPercentageToDP('2%'),
        marginBottom: 5,
    }
  })
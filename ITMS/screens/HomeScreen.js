import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen'
import Timetable  from './Content/Timetable'
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { Avatar } from 'react-native-paper'
import { connect } from 'react-redux'
import { makeGetRequest } from './utils.js'
import { storeUserInfo } from './Data/LoginReducer'
import { ROOT_IMAGE_URL } from './utils'

const TopSlider = createAppContainer(
  createSwitchNavigator({
    Main: Timetable,
  })
);

class HomeScreen extends Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props){
    super(props)
    this.state = {
      timetable: []
    }
  }

  async componentDidMount() {

    if (this.props.user.user_type == 'student') {
      const response = await makeGetRequest('session/get/timetable?course='+
      this.props.user.userprofile.course +'&level='+ this.props.user.userprofile.level )

      this.setState({
        timetable : response
       })
    }
    
    
  }



  render() {

    const user = this.props.user
    return (
      <View style={styles.container}>
        <View style={styles.top_section}>
          <Text style={styles.header_text}>
            { user.first_name + " " + user.last_name}
          </Text>
          <Text style={styles.section_description}>
              { user.registration_number }
          </Text>
          <Text style={styles.section_description}>
              {
                (user.user_type == 'student') ?
                user.userprofile.slug : user.email
              }
          </Text>
          <Text style={styles.section_description}>
              { user.user_type }
          </Text>
        </View>
        <View style={styles.main_section}>
           <TopSlider />
        </View>
        <View style={styles.header}>
          <Avatar.Image size={140} source={
                ( user.user_type == 'student') ?
                {uri: ROOT_IMAGE_URL + user.userprofile.profile_image } :
                 require('../assets/images/logo.png') }/>

        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    backgroundColor: '#47bac1',
    flex: 1
  },
  top_section: {
    flex: 1,
    backgroundColor: '#47bac1'
  },
  top_section_lecturer: {
    flex: 1,
    backgroundColor: '#7952B3'
  },
  header: {
    position: 'absolute',
    alignItems: 'center',
    backgroundColor: '#47bac1',
    borderRadius: 300,
    padding: 4,
    marginLeft: widthPercentageToDP('58%'),
    marginTop: heightPercentageToDP('4%')
  },
  main_section: {
    flex: 4,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    backgroundColor: '#0a0a0a',
    paddingTop: heightPercentageToDP('5.8%')
  },
  header_text : {
    fontFamily: 'space-mono',
    color: '#FFF',
    fontSize: heightPercentageToDP('2.8%'),
    marginTop: heightPercentageToDP('4%'),
    marginLeft: widthPercentageToDP("5%"),
  },
  small_header_text : {
    fontFamily: 'space-mono',
    color: '#7952b3c2',
    fontSize: heightPercentageToDP('4%'),
  },
  section_description: {
    fontFamily: 'space-mono',
    color: '#FFF',
    marginLeft: widthPercentageToDP('5%'),
    fontSize: heightPercentageToDP('2.2%'),
    marginTop: 5
  }
})

const mapStateToProps = state => {
  let user = state.user
  return {
    user: user
  };
};

const mapDispatchToProps = {
  storeUserInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
